<?php

namespace DreamCat\SwooleHttp2Psr\impl;

use DreamCat\SwooleHttp2Psr\RequestConverter;
use Laminas\Diactoros\ServerRequest;
use Laminas\Diactoros\StreamFactory;
use Psr\Http\Message\ServerRequestInterface;
use Swoole\Http\Request;
use function Laminas\Diactoros\marshalMethodFromSapi;
use function Laminas\Diactoros\marshalProtocolVersionFromSapi;
use function Laminas\Diactoros\marshalUriFromSapi;
use function Laminas\Diactoros\normalizeUploadedFiles;

/**
 * swoole请求对象到psr的转换器
 * @author vijay
 */
class DefaultRequestConvert implements RequestConverter
{
    /**
     * 将 swoole 的请求对象转为 psr 标准对象
     * @param Request $swooleRequest swoole的请求对象
     * @return ServerRequestInterface psr标准对象
     */
    public function convert(Request $swooleRequest): ServerRequestInterface
    {
        $server = $this->normalizeServer($swooleRequest->server);
        $files = $this->normalizeUploadedFiles($swooleRequest->files ?? []);
        $headers = $this->normalizeHeader($swooleRequest->header ?? []);

        return new ServerRequest(
            $server,
            $files,
            marshalUriFromSapi($server, $headers),
            marshalMethodFromSapi($server),
            (new StreamFactory())->createStream($swooleRequest->rawContent()),
            $headers,
            $swooleRequest->cookie ?? [],
            $swooleRequest->get ?? [],
            $swooleRequest->post,
            marshalProtocolVersionFromSapi($server)
        );
    }

    /**
     * 标准化 header
     * @param array $swooleHeaders
     * @return array
     */
    private function normalizeHeader(array $swooleHeaders): array
    {
        $headers = [];
        foreach ($swooleHeaders ?? [] as $key => $value) {
            $headers[ucwords($key, "-")] = $value;
        }
        return $headers;
    }

    /**
     * server变量标准化
     * @param array $server swoole请求中的server
     * @return array 转换后的结果
     * @note swoole 文档中说server变量全用了小写key，这与原本的fpm体系不一致
     */
    private function normalizeServer(array $server)
    {
        return $this->arrayKeyToUpper($server);
    }

    /**
     * 将数组的键转炎大写
     * @param array $array 原数组
     * @return array 转换后的数组
     */
    private function arrayKeyToUpper(array $array): array
    {
        $result = [];
        foreach ($array as $key => $item) {
            $result[strtoupper($key)] = $item;
        }
        return $result;
    }

    /**
     * files变量标准化
     * @param array $files
     * @return array|\Psr\Http\Message\UploadedFileInterface[]
     */
    private function normalizeUploadedFiles(array $files)
    {
        return normalizeUploadedFiles($this->arrayKeyToUpper($files));
    }
}

# end of file
