<?php

namespace DreamCat\SwooleHttp2Psr;

use Laminas\HttpHandlerRunner\Emitter\EmitterInterface;
use Psr\Http\Message\ResponseInterface;
use Swoole\Http\Response;

/**
 * swoole 版本的 response 处理器
 * @author vijay
 */
class SwooleEmit implements EmitterInterface
{
    /** @var Response swoole的响应对象 */
    private $swooleResponse;

    /**
     * SwooleEmit constructor.
     * @param Response $swooleResponse swoole响应对象
     */
    public function __construct(Response $swooleResponse)
    {
        $this->swooleResponse = $swooleResponse;
    }

    /**
     * 发出响应
     * 根据环境发出响应，包含状态行、报头和消息体。
     *
     * 实现这个方法可能会产生副作用，比如调用 header() 方法或将输出推送到缓冲区
     *
     * 如果无法发出消息，比如报头已发送，可以抛出异常
     *
     * 实现必须返回一个布尔值，表示是否成功发送
     * @param ResponseInterface $response 响应消息
     * @return bool
     */
    public function emit(ResponseInterface $response): bool
    {
        # 输出状态行
        $this->emitStatusLine($response);
        # 发出报头
        $this->emitHeaders($response);
        # 输出body
        $this->emitBody($response);
        return true;
    }

    /**
     * 发出报头
     *
     * 循环输出每个报头。
     * 如果报头值是一个多元素的数组，应该确保以聚合的形式发出报头而不是替换前一个
     * @param ResponseInterface $response 响应消息
     * @return void
     */
    private function emitHeaders(ResponseInterface $response): void
    {
        foreach ($response->getHeaders() as $header => $values) {
            $this->swooleResponse->header($header, implode(",", $values));
        }
    }

    /**
     * 输出状态行
     *
     * @param ResponseInterface $response 响应消息
     * @return void
     */
    private function emitStatusLine(ResponseInterface $response): void
    {
        $reasonPhrase = $response->getReasonPhrase();
        $statusCode   = $response->getStatusCode();
        $this->swooleResponse->status($statusCode, $reasonPhrase);
    }

    /**
     * 输出body
     * @param ResponseInterface $response 响应消息
     * @return void
     */
    private function emitBody(ResponseInterface $response): void
    {
        $this->swooleResponse->end($response->getBody()->__toString());
    }
}

# end of file
