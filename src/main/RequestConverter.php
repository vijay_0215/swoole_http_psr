<?php

namespace DreamCat\SwooleHttp2Psr;

use Psr\Http\Message\ServerRequestInterface;
use Swoole\Http\Request;

/**
 * 将 swoole 的请求对象转为 psr 标准对象
 * @author vijay
 */
interface RequestConverter
{
    /**
     * 将 swoole 的请求对象转为 psr 标准对象
     * @param Request $swooleRequest swoole的请求对象
     * @return ServerRequestInterface psr标准对象
     */
    public function convert(Request $swooleRequest): ServerRequestInterface;
}

# end of file
