<?php

namespace DreamCat\SwooleHttp2Psr\Helper;

use DreamCat\SwooleHttp2Psr\impl\DefaultRequestConvert;
use DreamCat\SwooleHttp2Psr\SwooleEmit;
use Laminas\Diactoros\Response\JsonResponse;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;
use Swoole\Process;

/**
 * http服务器
 * @author vijay
 */
class HttpServer
{
    /** @var Server $server 服务器对象句柄 */
    private $server;
    /** @var Process 子进程对象 */
    private $process;

    /**
     * HttpServer constructor.
     * @param Process $process 子进程对象
     */
    public function __construct(Process $process)
    {
        $this->process = $process;
        $this->process->name("测试用的服务器主进程");
    }

    /**
     * 创建http服务器
     * @param Process $process 子进程对象
     * @return void
     */
    public static function createHttpServer(Process $process)
    {
        (new self($process))->init()->start();
    }

    /**
     * 启用服务器
     * @return void
     */
    private function start(): void
    {
        $this->server->start();
    }

    /**
     * 初始化服务器
     * @return $this 对象本身
     */
    private function init(): HttpServer
    {
        $this->server = new Server("127.0.0.1");
        $this->server->set(
            [
                'log_file' => '/dev/null',
                'worker_num' => 1,
            ]
        );
        $this->server->on(
            "start",
            function () {
                $this->process->write($this->server->port);
            }
        );
        $this->server->on(
            "request",
            [
                $this,
                "onRequest",
            ]
        );
        return $this;
    }

    /**
     * 响应处理程序
     * @param Request $request swoole请求对象
     * @param Response $response swoole响应对象
     * @return void
     */
    public function onRequest(Request $request, Response $response)
    {
        $psrRequest = (new DefaultRequestConvert)->convert($request);
        $requestData = [
            "rawContent" => strval($psrRequest->getBody()),
            "method" => $psrRequest->getMethod(),
            "requestTarget" => $psrRequest->getRequestTarget(),
            "query" => $psrRequest->getUri()->getQuery(),
            "headers" => $psrRequest->getHeaders(),
            "path" => $psrRequest->getUri()->getPath(),
            "parseBody" => $psrRequest->getParsedBody(),
            "cookie" => $psrRequest->getCookieParams(),
            "files" => $psrRequest->getUploadedFiles(),
        ];
        $orgData = [
            "server" => $request->server,
            "header" => $request->header,
            "cookie" => $request->cookie,
            "get" => $request->get,
            "post" => $request->post,
            "files" => $request->files,
            "rawContent" => $request->rawContent(),
            "data" => $request->getData(),
        ];
        $psrResponse = new JsonResponse(
            [
                "psr" => $requestData,
                "_debug" => $orgData,
            ]
        );
        if (isset($request->get["expectCode"])) {
            $psrResponse = $psrResponse->withStatus($request->get["expectCode"]);
        }
        (new SwooleEmit($response))->emit($psrResponse);
    }
}

# end of file
