<?php

namespace DreamCat\SwooleHttp2Psr\Cases;

use DreamCat\SwooleHttp2Psr\Helper\HttpServer;
use PHPUnit\Framework\TestCase;
use Swoole\Process;

/**
 * 常规性测试
 * @author vijay
 */
class NormalTest extends TestCase
{
    /** @var Process 子进程句柄 */
    private static $process;
    /** @var int 子进程使用的http端口号 */
    private static $httpPort;

    /**
     * 安装类基境
     * @return void
     */
    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        # 启动新进程，开启http服务器
        self::$process = new Process(
            [
                HttpServer::class,
                "createHttpServer",
            ]
        );
        self::$process->start();
        self::$httpPort = self::$process->read();

        echo "测试http服务器已经开启，端口号：" . self::$httpPort . PHP_EOL;
    }

    /**
     * 取消类基境
     * @return void
     */
    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        # 关闭子进程
        Process::kill(self::$process->pid);
    }

    /**
     * 发送get请求，不带cookie
     * @return void
     */
    public function testSendGetDataWithoutCookie()
    {
        $expect = $this->genExpect("/test", "GET", ["a" => uniqid("qa-")]);
        $output = $this->sendData($expect);
        self::assertEquals($expect, $output["psr"] ?? null, __FUNCTION__ . "请求返回不合法");
    }

    /**
     * 生成预期输出
     * @param string $uri uri
     * @param string $method method
     * @param array $query get参数
     * @param array $cookies cookie信息
     * @param array $postData 请求的数据
     * @param bool $useJson 使用 json
     * @return array
     */
    private function genExpect(
        string $uri = "/",
        string $method = "GET",
        array $query = [],
        array $cookies = [],
        array $postData = [],
        bool $useJson = false
    ): array {
        $expect = [
            "rawContent" => "",
            "method" => $method,
            "query" => http_build_query($query),
            "requestTarget" => $uri,
            "headers" => [
                "Host" => ["127.0.0.1:" . self::$httpPort],
                "User-Agent" => ["Mozilla/5.0 (Windows NT 5.1; rv:24.0) Gecko/20100101 Firefox/24.0"],
                "Accept" => ["*/*"],
                "User-Defined-Header" => [uniqid("udh-")],
            ],
            "path" => $uri,
            "parseBody" => null,
            "cookie" => $cookies,
            "files" => [],
        ];
        if ($expect["query"]) {
            $expect["requestTarget"] .= "?{$expect["query"]}";
        }
        if ($method == "POST") {
            if ($useJson) {
                $expect["headers"]["Content-Type"] = ["application/json"];
                $expect["rawContent"] = json_encode($postData);
            } else {
                $expect["headers"]["Content-Type"] = ["application/x-www-form-urlencoded"];
                $expect["rawContent"] = http_build_query($postData);
                $expect["parseBody"] = $postData;
            }
            $expect["headers"]["Content-Length"] = [strlen($expect["rawContent"])];
        }
        return $expect;
    }

    /**
     * 根据预期输出发送消息
     * @param array $expect 预期输出
     * @return mixed 实际输出
     */
    private function sendData(array $expect)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://{$expect["headers"]["Host"][0]}{$expect["requestTarget"]}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (isset($expect["headers"]["User-Agent"][0])) {
            curl_setopt(
                $ch,
                CURLOPT_USERAGENT,
                $expect["headers"]["User-Agent"][0]
            );
        }
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array_map(
                function ($key) use ($expect) {
                    return "{$key}: " . implode(",", $expect["headers"][$key]);
                },
                array_keys(
                    array_filter(
                        $expect["headers"],
                        function ($key) {
                            return $key != "Host" && $key != "User-Agent" && $key = "Accept";
                        },
                        ARRAY_FILTER_USE_KEY
                    )
                )
            )
        );
        if ($expect["cookie"]) {
            $cookie = [];
            foreach ($expect["cookie"] as $key => $val) {
                $cookie[] = "{$key}={$val}";
            }
            curl_setopt($ch, CURLOPT_COOKIE, implode(";", $cookie));
        }
        if ($expect["method"] == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            if ($expect["rawContent"]) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $expect["rawContent"]);
            } elseif (isset($expect["files"])) {
                $post = $expect["parseBody"] ?? [];
                foreach ($expect["files"] as $key => $val) {
                    $post[$key] = new \CURLFile($val);
                }
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            } elseif ($expect["parseBody"]) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $expect["parseBody"]);
            }
        }
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output, true);
    }

    /**
     * 发送get消息，带cookie
     * @return void
     */
    public function testSendGetDataWithCookie()
    {
        $expect = $this->genExpect(
            "/testcookie",
            "GET",
            ["t" => uniqid("qt-")],
            [
                "ck1" => uniqid("ck1-"),
                "tm" => strval(microtime(true)),
            ]
        );
        $output = $this->sendData($expect);
        self::assertEquals($expect, $output["psr"] ?? null, __FUNCTION__ . "请求返回不合法");
    }

    /**
     * 发送form表单数据，不带cookie
     * @return void
     */
    public function testSendFormDataWithoutCookie()
    {
        $expect = $this->genExpect(
            "/testform",
            "POST",
            ["pf" => uniqid("qpf-")],
            [],
            [
                "field" => "abc",
                "k2" => uniqid("-k2-"),
            ]
        );
        $output = $this->sendData($expect);
        self::assertEquals($expect, $output["psr"] ?? null, __FUNCTION__ . "请求返回不合法");
    }

    /**
     * 发送form表单数据，带cookie
     * @return void
     */
    public function testSendFormDataWithCookie()
    {
        $expect = $this->genExpect(
            "/testformAndCookie",
            "POST",
            [],
            ["ck" => "abc-" . uniqid("ck-")],
            [
                "field" => "abc",
                "k2" => uniqid("-k2-"),
            ]
        );
        $output = $this->sendData($expect);
        self::assertEquals($expect, $output["psr"] ?? null, __FUNCTION__ . "请求返回不合法");
    }

    /**
     * 不带cookie发送json请求
     * @return void
     */
    public function testSendJsonData()
    {
        $expect = $this->genExpect(
            "/testjson",
            "POST",
            [],
            [],
            [
                "field" => "abc",
                "k2" => uniqid("-k2-"),
            ],
            true
        );
        $output = $this->sendData($expect);
        self::assertEquals($expect, $output["psr"] ?? null, __FUNCTION__ . "请求返回不合法");
    }

    /**
     * 不带cookie上传文件
     * @return void
     */
    public function testSendFileData()
    {
        $expect = $this->genExpect(
            "/testfile",
            "GET"
        );
        # 文件提交的变化较多，还是手动修改吧
        $expect["method"] = "POST";
        $expect["files"] = ["f" => realpath(__DIR__ . "/../bootstrap.php")];
        $expect["parseBody"] = ["body" => uniqid("body-")];
        $output = $this->sendData($expect);

        $diff = [
            "rawContent" => $output["psr"]["rawContent"] ?? null,
            "cl" => $output["psr"]["headers"]["Content-Length"] ?? null,
            "ct" => $output["psr"]["headers"]["Content-Type"] ?? null,
            "files" => [
                "o" => $output["psr"]["files"] ?? null,
                "e" => $expect["files"],
            ],
        ];
        unset($expect["rawContent"]);
        unset($output["psr"]["rawContent"]);
        unset($output["psr"]["headers"]["Content-Length"]);
        unset($output["psr"]["headers"]["Content-Type"]);
        unset($expect["files"]);
        unset($output["psr"]["files"]);

        # 判断预期一致的信息
        self::assertEquals($expect, $output["psr"] ?? null, __FUNCTION__ . "请求返回不合法");
        # 判断预期不完全一致的信息
        self::assertEquals([strlen($diff["rawContent"])], $diff["cl"], "文件请求内容长度不正确");
        self::assertEquals(
            "multipart/form-data; boundary=------------------------",
            substr($diff["ct"][0] ?? "", 0, 54),
            "文件请求内容类型不正确"
        );
        $id = substr($diff["ct"][0], 54);
        $rawContent = "--------------------------{$id}\r\n";
        $rawContent .= "Content-Disposition: form-data; name=\"body\"\r\n\r\n";
        $rawContent .= "{$expect["parseBody"]["body"]}\r\n";
        $rawContent .= "--------------------------{$id}\r\n";
        $rawContent .= "Content-Disposition: form-data; name=\"f\"; filename=\"{$diff["files"]["e"]["f"]}\"\r\n";
        $rawContent .= "Content-Type: application/octet-stream\r\n";
        $rawContent .= "\r\n" . file_get_contents($diff["files"]["e"]["f"]);
        $rawContent .= "\r\n--------------------------{$id}--\r\n";
        self::assertEquals($rawContent, $diff["rawContent"], "文件请求 rawContent 不正确");
    }
}

# end of file
